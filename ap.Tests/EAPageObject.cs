﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Safari;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

namespace ap.Tests
{
    class EAPageObject
    {


        public EAPageObject()
        {
            PageFactory.InitElements(ProperetiesCollection.driver,this);
        }

        //elements of devis
        //
        [FindsBy(How=OpenQA.Selenium.Support.PageObjects.How.Name,Using = "email")]
        public IWebElement txtEmail { get; set; }
       
        //
        [FindsBy(How=OpenQA.Selenium.Support.PageObjects.How.Id, Using = "form-password")]
        public IWebElement txtPassword { get; set; }

        //
        [FindsBy(How = OpenQA.Selenium.Support.PageObjects.How.Id, Using = "devis-btn")]
        public IWebElement devisButton { get; set; }

        // 
        [FindsBy(How = OpenQA.Selenium.Support.PageObjects.How.Id, Using = "designtaionA2")]
        public IWebElement txtDesignationA2 { get; set; }

        // 
        [FindsBy(How = OpenQA.Selenium.Support.PageObjects.How.Id, Using = "prixVA")]
        public IWebElement txtPrixVA { get; set; }

        // 
        [FindsBy(How = OpenQA.Selenium.Support.PageObjects.How.Id, Using = "prixVA")]
        public IWebElement txtPrixV { get; set; }

        // 
        [FindsBy(How = OpenQA.Selenium.Support.PageObjects.How.Id, Using = "quantite")]
        public IWebElement txtQuantite { get; set; }
        /*
        // 
        [FindsBy(How = OpenQA.Selenium.Support.PageObjects.How.Id, Using = "uniteA2")]
        public IWebElement txtUniteA2 { get; set; }

        // 
        [FindsBy(How = OpenQA.Selenium.Support.PageObjects.How.Id, Using = "classA2")]
        public IWebElement txtClassA2 { get; set; }

        // 
        [FindsBy(How = OpenQA.Selenium.Support.PageObjects.How.Id, Using = "forunisseurA2")]
        public IWebElement txtforunisseurA2 { get; set; }
  */
        //
        [FindsBy(How = OpenQA.Selenium.Support.PageObjects.How.CssSelector, Using = "input.btn.btn-se-connecter.col-sm-12.col-xs-12")]
        //[FindsBy(How = OpenQA.Selenium.Support.PageObjects.How.CssSelector, Using = "body > div.wrapper > div > div > div > div > div > div > div.form-bottom > form > input")]
        public IWebElement Seconnecter { get; set; }

        //elements of facture
        //.....
    }
}