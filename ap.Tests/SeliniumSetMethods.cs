﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading.Tasks;

namespace ap.Tests
{
     class SeliniumSetMethods
    {
        // Inter Text  (element, value, type)

        public static void InterText(string element, string value, How elementtype)
        {
            if (elementtype == How.Id)
                ProperetiesCollection.driver.FindElement(By.Id(element)).SendKeys(value+Keys.Enter);

            if (elementtype == How.Name)
                ProperetiesCollection.driver.FindElement(By.Name(element)).SendKeys(value+Keys.Enter);
            
            if (elementtype == How.CssSelector)
                ProperetiesCollection.driver.FindElement(By.CssSelector(element)).SendKeys(value + Keys.Enter);
            
          
        }

        // click into a button, checkbox, option etc

        public static void Click(string element, How elementtype)
        {
            if (elementtype == How.Id)
                ProperetiesCollection.driver.FindElement(By.Id(element)).Click();

            if (elementtype == How.Name)
                ProperetiesCollection.driver.FindElement(By.Name(element)).Click();
            
            if (elementtype == How.CssSelector)
                ProperetiesCollection.driver.FindElement(By.CssSelector(element)).Click();
        }

        // Selecting a drop down control

        public static void SelectDropDown(string element, string value, How elementtype)
        {
            if (elementtype == How.Id)
                new SelectElement(ProperetiesCollection.driver.FindElement(By.Id(element))).SelectByText(value);

            if (elementtype == How.Name)
                new SelectElement(ProperetiesCollection.driver.FindElement(By.Name(element))).SelectByText(value);
            
            if (elementtype == How.ClassName)
                new SelectElement(ProperetiesCollection.driver.FindElement(By.ClassName(element))).SelectByText(value);
            
            if (elementtype == How.CssSelector)
                new SelectElement(ProperetiesCollection.driver.FindElement(By.CssSelector(element))).SelectByText(value);
            
        }


       
    }
}
