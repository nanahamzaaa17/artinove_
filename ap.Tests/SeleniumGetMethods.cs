﻿using System;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
namespace ap.Tests
{
   class SeleniumGetMethods
    {
        public static string GetText(string element, How elementtype)
        {
            if (elementtype == How.Id)
                return ProperetiesCollection.driver.FindElement(By.Id(element)).GetAttribute("value");

            if (elementtype == How.Name)
                return ProperetiesCollection.driver.FindElement(By.Name(element)).GetAttribute("value");
            else return String.Empty;
        }

        public static string GetTextDDL(string element, How elementtype)
        {
            if (elementtype == How.Id)
                return new SelectElement(ProperetiesCollection.driver.FindElement(By.Id(element))).AllSelectedOptions.SingleOrDefault().Text;

            if (elementtype == How.Name)
                return new SelectElement(ProperetiesCollection.driver.FindElement(By.Name(element))).AllSelectedOptions.SingleOrDefault().Text;

            else return String.Empty;

        }
        /*
        public static IWebElement FindElement(IWebProperetiesCollection.driver ProperetiesCollection.driver, string attribute, string attributeValue)
        {
            switch (attribute)
            {
                case "Id":
                    return ProperetiesCollection.driver.FindElement(By.Id(attributeValue));
                case "Name":
                    return ProperetiesCollection.driver.FindElement(By.Name(attributeValue));
                default:
                    throw new NoSuchElementException(string.Format("Cannont find element with attribute {0} and attributeValue {1}", attribute, attributeValue));
            }
        }﻿
        */
    }
}
