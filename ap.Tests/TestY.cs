﻿using System;
using NUnit.Framework;
namespace ap.Tests
{
    [TestFixture]
    [Ignore("Skip the Test")]// pour ignorer tous le test
    public class TestY
    {
        [Test,TestCaseSource("RegisterData")] // TestRegester is a test case and the name of function source data 

        public void TestRegister(string username, string password, string email, string city)
        {
            //code selenium these function will be called many time with different sets of data :we use parametrization
            //username
            //password
            //email
            //city

            Console.WriteLine(username + "--" + password + "--" + email + "--" + city + "--");
        }

        // data source
        public static object[] RegisterData()
        {
            //rows -no of times has to be executed
            //cols -no of parametrs in data
            object[][] data = new object[3][];

            //first row
            data[0] = new object[4];
            data[0][0] = "user1";
            data[0][1] = "pass1";
            data[0][2] = "email";
            data[0][3] = "city1";

            //second row
            data[1] = new object[4];
            data[1][0] = "user2";
            data[1][1] = "pass2";
            data[1][2] = "email2";
            data[1][3] = "city2";

            //third row
            data[2] = new object[4];
            data[2][0] = "user3";
            data[2][1] = "pass3";
            data[2][2] = "email3";
            data[2][3] = "city3";

            return data;
        }
    }
}