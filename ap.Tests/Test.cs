﻿using NUnit.Framework;
using System;

namespace ap.Tests
{
    [TestFixture]
    [Ignore("Skip the Test")]// pour ignorer tous le test
    public class Test
    {

        [SetUp]//befor executing every test case
        public void setup()
        {
            //code selenium
            Console.WriteLine("opening browsers");
        }

        [Test] // Test App is a test case
        public void TestreceiveEmail()
        {    
            //code selenium
            Console.WriteLine("Testing receive email");
        }

        [Test] // TestLogin is a test case
        public void TestSendEmail()
        {
            //code selenium
            Console.WriteLine("Testing sending email");
        }

        [TearDown]//afer executing every test case
        public void closeBrowser()
        {
            //code selenium
            Console.WriteLine("close browsers");
        }

    }
}
