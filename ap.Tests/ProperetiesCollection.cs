﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Safari;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Support.UI;
//using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;
using System.Threading.Tasks;

namespace ap.Tests
{
    enum How
    {
        Id,
        Name,
        TagName,
        ClassName,
        CssSelector,
        LinkText,
        PartialLinkText,
        XPath,
        Custom
    }
      class ProperetiesCollection// used if you dont need to pass driver in parameter of the methods 
    {
        public static IWebDriver driver {
            get;
            set;
        }
    }
}
