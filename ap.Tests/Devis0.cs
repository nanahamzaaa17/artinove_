﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Safari;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;
//using System.Threading.Tasks;

namespace ap.Tests
{
    [TestFixture]
    //[Parallelizable]
    //[Ignore("Skip the Test")]

    public class Devis0
    {
        //IWebDriver driver;

        [SetUp]
        public void Initialize()
        {
            //driver = new SafariDriver();
            ProperetiesCollection.driver = new ChromeDriver();
            ProperetiesCollection.driver.Navigate().GoToUrl("https://www.artinove.fr");
            Console.WriteLine("Testing Artinove website");
        }

        [Test] // Test App is a test case0: click sur devis et click sur acceuil

        public void ExecuteTest0_connexion()
        {
            //connexion
            //Initialize the page by calling reference
            SeliniumSetMethods.Click("body > header > div > div > nav > div > a.btn.btn-connect", How.CssSelector);
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.InterText("email", "nejmahamzaoui@gmail.com", How.Name);
            SeliniumSetMethods.InterText("form-password", "artinove ", How.Id);


            //SeliniumSetMethods.InterText("email", "nejmahamzaoui@gmail.com", How.Name);
            //SeliniumSetMethods.InterText("form-password", "artinove ", How.Id);
        }

        [Test] // Test App is a test case0: click sur devis et click sur acceuil

        public void ExecuteTest1_ClickDevis()
        {
            //connexion
            //Initialize the page by calling reference
            SeliniumSetMethods.Click("body > header > div > div > nav > div > a.btn.btn-connect", How.CssSelector);
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.InterText("email", "nejmahamzaoui@gmail.com", How.Name);
            SeliniumSetMethods.InterText("form-password", "artinove ", How.Id);

            //click sur devis et retour a l acceuil
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.Click("devis-btn", How.Id);
            //driver.FindElement(By.LinkText("Devis")).Click();

            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            //SeliniumSetMethods.Click(driver, "button.btn.btn-success", "CssSelector");
            //driver.FindElement(By.CssSelector("button.btn.btn-success")).Click();
            // retour a Acceuil
            SeliniumSetMethods.Click("button.btn.btn-primary[ng-click *='home']", How.CssSelector);
            //driver.FindElement(By.CssSelector("button.btn.btn-primary[ng-click *='home']")).Click();
            //driver.FindElement(By.CssSelector("body > div.wrapper > div > div > div > section.content-header.ng-scope > nav > button.btn.btn-primary")).Click();
        }

        [Test] // Test App is a test case2:click sur devis et lot 

        public void ExecuteTest2_ClickLots()
        {
            //Assert.Ignore("skip this method");// pour ingnorer juste le test de cette methode
            //connexion
            //Initialize the page by calling reference
            SeliniumSetMethods.Click("body > header > div > div > nav > div > a.btn.btn-connect", How.CssSelector);
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.InterText("email", "nejmahamzaoui@gmail.com", How.Name);
            SeliniumSetMethods.InterText("form-password", "artinove ", How.Id);

            //creation devis
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.Click("devis-btn", How.Id);
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.Click("button.btn.btn-success", How.CssSelector);
            //driver.FindElement(By.CssSelector("button.btn.btn-success")).Click();
            // ajouter lot
            // driver.FindElement(By.CssSelector("button.btn.btn-primary.btnSame[ng-click *='addLots']")).Click();
            SeliniumSetMethods.Click("button.btn.btn-primary.btnSame[ng-click *='addLots']", How.CssSelector);
        }

        [Test] // Test App is a test case3: creation de devis y compris article,client
        public void ExecuteTest3_CreationDevis()
        {
            //Assert.Ignore("skip this method");// pour ingnorer juste le test de cette methode
            //connexion

            //Initialize the page by calling reference
            SeliniumSetMethods.Click("body > header > div > div > nav > div > a.btn.btn-connect", How.CssSelector);
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.InterText("email", "nejmahamzaoui@gmail.com", How.Name);
            SeliniumSetMethods.InterText("form-password", "artinove ", How.Id);

            //creation devis
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.Click("devis-btn", How.Id);
            //driver.FindElement(By.LinkText("Devis")).Click();
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            SeliniumSetMethods.Click("button.btn.btn-success", How.CssSelector);
            //driver.FindElement(By.CssSelector("button.btn.btn-success")).Click();

            //saisir article
            //click sur article
            SeliniumSetMethods.Click("button.btn.btn-success.btnSame", How.CssSelector);
            //driver.FindElement(By.CssSelector("button.btn.btn-success.btnSame")).Click();

            //entrer les donnees
            SeliniumSetMethods.InterText("designtaionA2", "article1", How.Id);
            SeliniumSetMethods.InterText("prixVA", "10", How.Id);
            SeliniumSetMethods.InterText("prixVA", "20", How.Id);

            SeliniumSetMethods.InterText("quantite", "30", How.Id);
            SeliniumSetMethods.SelectDropDown("uniteA2", "U", How.Id);
            SeliniumSetMethods.SelectDropDown("classA2", "Produit", How.Id);
            SeliniumSetMethods.SelectDropDown("forunisseurA2", "ACER", How.Id);

            //valider
            SeliniumSetMethods.Click("button.btn.btn-success[ng-click *='ajouterArticle2(article)']", How.CssSelector);
            //driver.FindElement(By.CssSelector("button.btn.btn-success[ng-click *='ajouterArticle2(article)']")).Click();
            //driver.FindElement(By.CssSelector("body>div.modal.ng-scope div>div.modal-footer>button.btn.btn-success")).Click();

            //choix client
            //driver.FindElement(By.CssSelector("input.form-control.ng-pristine.ng-valid.ng-touched")).Click();
            SeliniumSetMethods.Click("#clientTuto > div.col-md-8.col-sm-7.col-xs-7.text-left > div > input", How.CssSelector);
            //driver.FindElement(By.CssSelector("#clientTuto > div.col-md-8.col-sm-7.col-xs-7.text-left > div > input")).Click();
            //driver.FindElement(By.CssSelector("body > div.wrapper > div > div > div > section.content.ng-scope > div.row > div > ul > li:nth-child(2) > a > i")).Click();
            SeliniumSetMethods.Click("#modalWinHeight4>div:nth-child(3)>div>div.col-sm-2>input", How.CssSelector);
            //driver.FindElement(By.CssSelector("#modalWinHeight4>div:nth-child(3)>div>div.col-sm-2>input")).Click();

            //valider devis
            SeliniumSetMethods.Click("button.btn.btn-success[ng-click *='devis.status=0;validerDevis(devis)']", How.CssSelector);
            //driver.FindElement(By.CssSelector("button.btn.btn-success[ng-click *='devis.status=0;validerDevis(devis)']")).Click();
        }



        [Test] // Test App is a test case4: annuler un devis
        public void ExecuteTest4_AnnulerDevis()

        {   //Assert.Ignore("skip this method");// pour ingnorer juste le test de cette methode


            //Initialize the page by calling reference
            SeliniumSetMethods.Click("body > header > div > div > nav > div > a.btn.btn-connect", How.CssSelector);
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.InterText("email", "nejmahamzaoui@gmail.com", How.Name);
            SeliniumSetMethods.InterText("form-password", "artinove ", How.Id);

            //creation devis
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.Click("devis-btn", How.Id);
            //driver.FindElement(By.LinkText("Devis")).Click();
            ProperetiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            SeliniumSetMethods.Click("button.btn.btn-success", How.CssSelector);
            //driver.FindElement(By.CssSelector("button.btn.btn-success")).Click();

            //saisir article
            //click sur article
            //driver.FindElement(By.CssSelector("button.btn.btn-success.btnSame")).Click();
            SeliniumSetMethods.Click("button.btn.btn-success.btnSame", How.CssSelector);
            //SeliniumSetMethods.Click("button.btn.btn-success.btnSame", How.CssSelector);
            //entrer les donnees
            SeliniumSetMethods.InterText("designtaionA2", "article1", How.Id);
            SeliniumSetMethods.InterText("prixVA", "10", How.Id);
            SeliniumSetMethods.InterText("prixVA", "20", How.Id);

            SeliniumSetMethods.InterText("quantite", "30", How.Id);
            SeliniumSetMethods.SelectDropDown("uniteA2", "U", How.Id);
            SeliniumSetMethods.SelectDropDown("classA2", "Produit", How.Id);
            SeliniumSetMethods.SelectDropDown("forunisseurA2", "ACER", How.Id);

            //valider
            SeliniumSetMethods.Click("button.btn.btn-success[ng-click *='ajouterArticle2(article)']", How.CssSelector);
            // driver.FindElement(By.CssSelector("button.btn.btn-success[ng-click *='ajouterArticle2(article)']")).Click();
            //driver.FindElement(By.CssSelector("body>div.modal.ng-scope div>div.modal-footer>button.btn.btn-success")).Click();

            //choix client
            //driver.FindElement(By.CssSelector("input.form-control.ng-pristine.ng-valid.ng-touched")).Click();
            SeliniumSetMethods.Click("#clientTuto > div.col-md-8.col-sm-7.col-xs-7.text-left > div > input", How.CssSelector);
            //driver.FindElement(By.CssSelector("#clientTuto > div.col-md-8.col-sm-7.col-xs-7.text-left > div > input")).Click();
            //driver.FindElement(By.CssSelector("body > div.wrapper > div > div > div > section.content.ng-scope > div.row > div > ul > li:nth-child(2) > a > i")).Click();
            SeliniumSetMethods.Click("#modalWinHeight4>div:nth-child(3)>div>div.col-sm-2>input", How.CssSelector);
            //driver.FindElement(By.CssSelector("#modalWinHeight4>div:nth-child(3)>div>div.col-sm-2>input")).Click();

            //annuler devis
            SeliniumSetMethods.Click("body > div.wrapper > div > div > div > section.content.ng-scope > div.content.visible > div.main > nav > button.btn.btn-danger", How.CssSelector);
            //driver.FindElement(By.CssSelector("body > div.wrapper > div > div > div > section.content.ng-scope > div.content.visible > div.main > nav > button.btn.btn-danger")).Click();
            //driver.FindElement(By.CssSelector("body > div.modal.ng-scope > div > div > div > div.modal-footer > div > div:nth-child(2) > button")).Click();
            SeliniumSetMethods.Click("body > div.modal.ng-scope > div > div > div > div.modal-footer > div > div:nth-child(2) > button", How.CssSelector);
            //driver.FindElement(By.CssSelector("button.btn.btn-block.btn-danger[ng-click*='closeModal']")).Click();

        }

        [TearDown]
        public void CloseBrowser()
        {

            // driver.Close();
        }

    }
}


