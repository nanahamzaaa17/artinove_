﻿using System;
using NUnit.Framework;
using System.Threading.Tasks;
namespace ap.Tests
{
    [TestFixture]
    //[Parallelizable] pour faire les tests en paralell mais je crois que pr cette version ils se font parallement
    [Ignore("Skip the Test")]// pour ignorer tous le test
    public class YahooTest
    {
        [Test] // Testnews is a test case
        public void Testnews()
        {
            Console.WriteLine("befor Testing news ");
            //code selenium
            // I want to verify something , i well compare the expected and the actual value then we use assertions 
            //for example text is present , link is present in the application

            try
            {
                Assert.AreEqual("good", "goods");// if this part is failed the rest is not executed, and we passed to the next case test 
                                                 //, so in order to do this we make try ..execption to catch this erreur  

            }catch(Exception e){ Console.WriteLine("catch the error");}

            Console.WriteLine("after Testing news ");

            // click - assertion - we use try cath block : in the case we have to verify many links if opening or not we take www.quikr.com
         
        }


        [Test] // Test App is a test case
        public void TestX()
        {
            Assert.Ignore("skip this method");// pour ingnorer juste le test de cette methode
            //code selenium
            Console.WriteLine("Testing yahoo");

        }
       
    }
}
